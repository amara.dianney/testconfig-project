import { configService } from './config/config.service';
import { NestFactory } from "@nestjs/core";
import { AppModule } from "./app.module";
import { Logger } from "@nestjs/common";

const port = Number(configService.getPort());

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  await app.listen(port);
  Logger.log(`Server running in http://localhost:${port}`, "Bootsrap");
}
bootstrap();
