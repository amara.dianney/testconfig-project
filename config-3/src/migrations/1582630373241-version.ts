import {MigrationInterface, QueryRunner} from "typeorm";

export class version1582630373241 implements MigrationInterface {
    name = 'version1582630373241'

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `prospect` (`id` int NOT NULL AUTO_INCREMENT, `codeProspect` varchar(30) NULL, `typeSociete` int NULL, `raison_sociale` varchar(191) NULL, `email` varchar(191) NULL, `pays` varchar(191) NULL, `ville` varchar(191) NULL, `telephone` varchar(100) NULL, `sigle` varchar(100) NULL, `status` enum ('En cours', 'En attente') NULL DEFAULT 'En cours', `created_at` datetime(6) NULL DEFAULT CURRENT_TIMESTAMP(6), `updated_at` datetime(6) NULL DEFAULT CURRENT_TIMESTAMP(6), `created_by` varchar(100) NULL, `updated_by` varchar(100) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("DROP TABLE `prospect`", undefined);
    }

}
