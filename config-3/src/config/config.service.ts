import { Injectable } from '@nestjs/common';
import * as dotenv from "dotenv";
import * as fs from "fs";
import * as Joi from "@hapi/joi";
import { TypeOrmModuleOptions } from "@nestjs/typeorm";
import { Logger } from "@nestjs/common";

require("dotenv").config();

export type EnvConfig = Record<string, string>;

@Injectable()
class ConfigService {
  private readonly envConfig: EnvConfig;

  constructor(filePath: string) {
    const config = dotenv.parse(fs.readFileSync(filePath));
    this.envConfig = this.validateInput(config);
  }

  get(key: string): string {
    return this.envConfig[key];
  }

  public getTypeOrmConfig(): TypeOrmModuleOptions {
    return {
      type: "mysql",
      host: this.envConfig.TYPEORM_HOST,
      port: parseInt(this.envConfig.TYPEORM_PORT),
      username: this.envConfig.TYPEORM_USERNAME,
      password: this.envConfig.TYPEORM_PASSWORD,
      database: this.envConfig.TYPEORM_DATABASE,
      synchronize: false,
      migrationsTableName: "migrations",
      entities: ["src/**/*.entity{.ts,.js}"],
      migrations: ["src/migrations/*.ts"],
      cli: {
        migrationsDir: "src/migrations"
      }
    };
  }

  private validateInput(envConfig: EnvConfig): EnvConfig {
    const envVarsSchema: Joi.ObjectSchema = Joi.object({
      NODE_ENV: Joi.string()
        .valid("dev", "prod", "test", "staging")
        .default("dev"),
      PORT: Joi.number().default(3000),
      CORE_API: Joi.string().default("https://api-st.akilcab.com")
    }).unknown(true);

    const { error, value: validatedEnvConfig } = envVarsSchema.validate(
      envConfig
    );
    if (error) {
      throw new Error(`Config validation error: ${error.message}`);
    }
    return validatedEnvConfig;
  }
}

const configService = new ConfigService(
  `.env${process.env.NODE_ENV == null ? "" : `.${process.env.NODE_ENV}`}`
);
Logger.log(
  `App running with: .env${
    process.env.NODE_ENV == null ? "" : `.${process.env.NODE_ENV}`
  }`,
  "Environment"
);
export {configService};


